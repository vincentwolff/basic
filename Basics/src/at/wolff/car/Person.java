package at.wolff.car;

import java.util.ArrayList;
import java.util.List;

public class Person {

	private String firstName;
	private String lastName;
	private int birthday;
	private List<Car> cars;

	public Person(String firstName, String lastName, int birthday) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthday = birthday;
		this.cars = new ArrayList<Car>();
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getBirthday() {
		return birthday;
	}

	public void setBirthday(int birthday) {
		this.birthday = birthday;
	}
	
	public void addCar(Car cars) {
		this.cars.add(cars);
	}
	
	
	public void getValueOfCars() {
		int price = 0;
		for(Car counter : cars) {
			price = price + counter.getPrice();
		}
		System.out.println(price);
	}

}
