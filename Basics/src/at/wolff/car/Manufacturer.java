package at.wolff.car;

public class Manufacturer {

	private String name;
	private String origin;
	private int discount;

	public Manufacturer(String name, String origin, int discount) {
		this.name = name;
		this.origin = origin;
		this.discount = discount;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public int getDiscount() {
		return discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}

}