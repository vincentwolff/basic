package at.wolff.car;

public class Car {

	private String color;
	private int vMax;
	private int price;
	private double usage;
	private Engine motor;
	private Manufacturer manu;

	public Car(String color, int vMax, int price, double usage, Engine motor, Manufacturer manu) {
		this.color = color;
		this.vMax = vMax;
		this.price = price;
		this.usage = usage;
		this.motor = motor;
		this.manu = manu;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getVmax() {
		return vMax;
	}

	public void setVmax(int vMax) {
		this.vMax = vMax;
	}

	public int getPrice() {
		return (this.price - this.price / 100 * manu.getDiscount());
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public double getUsage() {
		return usage;
	}

	public void setUsage(double usage) {
		this.usage = usage;
	}

	public Engine getMotor() {
		return motor;
	}

	public void setMotor(Engine motor) {
		this.motor = motor;
	}

	public Manufacturer getManu() {
		return manu;

	}

	public void setManu(Manufacturer manu) {
		this.manu = manu;
	}

}
