package at.wolff.car;

public class Main {

	public static void main(String[] args) {

		Engine e1 = new Engine("Diesel", 200);
		
		Manufacturer m1 = new Manufacturer("Audi", "Deutschland", 10);
		Manufacturer m2 = new Manufacturer("Mercedes", "Deutschland",9);

		
		Car c1 = new Car("black", 200, 20000, 7.5, e1, m1);
		c1.getColor();
		c1.setManu(m2);
		
		Car c2 = new Car("white", 200, 55000, 7.5, e1, m1);
		c2.getColor();


		Person p1 = new Person("Pascal", "Konrad",1999);
		
		p1.addCar(c1);
		p1.addCar(c2);
		
		p1.getValueOfCars();
		

		
		//System.out.println("Ich koste mit einem Rabtt von " + c1.getManu().getDiscount() + "% " + c1.getPrice() + "€");
		
		
		

	}

}
