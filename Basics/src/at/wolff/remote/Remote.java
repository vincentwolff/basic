package at.wolff.remote;

import java.util.ArrayList;
import java.util.List;

public class Remote {

	private int length; // Millimeter
	private int width; // Millimeter
	private int heigth; // Millimeter
	private int weigth; // Gramm
	private String serialNumber;
	private boolean isOn = false;
	private List<Device> devices;

	public Remote(int length, int width, int heigth, int weigth, String serialNumber) {
		super();
		this.length = length;
		this.width = width;
		this.heigth = heigth;
		this.weigth = weigth;
		this.serialNumber = serialNumber;
		this.devices = new ArrayList<Device>();

	}

	public void addDevice(Device devices) {
		this.devices.add(devices);
	}

	public void turnOn() {
//		System.out.println("I am turned on");
		this.isOn = true;
	}

	public void turnOff() {
//		System.out.println("I am turned off");
		this.isOn = false;
	}

	public void myValues() {
		System.out.println("I am " + this.length + "mm long, " + this.width + "mm wide, " + this.heigth
				+ "mm high and I weigh " + this.weigth + ". My Serial number is " + this.serialNumber + ".");
	}

	public boolean isOn() {
		return isOn;
	}

	public void printStatus() {

	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeigth() {
		return heigth;
	}

	public void setHeigth(int heigth) {
		this.heigth = heigth;
	}

	public int getWeigth() {
		return weigth;
	}

	public void setWeigth(int weigth) {
		this.weigth = weigth;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public List<Device> getDevices() {
		return devices;
	}

	public void setDevices(List<Device> devices) {
		this.devices = devices;
	}

	public void setOn(boolean isOn) {
		this.isOn = isOn;
	}

//	public void whoIsOn() {
//		if() { System.out.println("Is turned on");
//	}

}