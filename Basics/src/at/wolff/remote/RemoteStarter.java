package at.wolff.remote;

public class RemoteStarter {

	public static void main(String[] args) {

		Device d1 = new Device("343hbj", Device.type.beamer);
		Device d2 = new Device("sdkfw3", Device.type.television);

		Remote r1 = new Remote(100, 30, 10, 200, "1001A");
		Remote r2 = new Remote(80, 200, 8, 250, "1002A");
		Remote r3 = new Remote(120, 35, 12, 300, "1003A");
		Remote r4 = new Remote(10, 30, 10, 200, "1004A");
		
		

		r1.turnOn();
		r2.turnOn();
		r4.turnOn();
		r2.turnOff();

		if (r1.isOn()) {
			System.out.println("Remote 1 is turned on");
		} else {
			System.out.println("Remote 1 is turned off");
		}

		if (r2.isOn()) {
			System.out.println("Remote 2 is turned on");
		} else {
			System.out.println("Remote 2 is turned off");
		}

		if (r3.isOn()) {
			System.out.println("Remote 3 is turned on");
		} else {
			System.out.println("Remote 3 is turned off");
		}

		if (r4.isOn()) {
			System.out.println("Remote 4 is turned on");
		} else {
			System.out.println("Remote 4 is turned off");
		}

	}

}
